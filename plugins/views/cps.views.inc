<?php

/**
 * @file
 * Views integration
 */

/**
 * Implements hook_views_data().
 */
function cps_views_data() {
  $data['cps_changeset_history']['table']['group'] = t('Changeset History');

  $data['cps_changeset_history']['table']['base'] = [
    'field' => 'changeset_id',
    'title' => t('Changeset History'),
    'help' => t('Table containing transition history for changesets.'),
    'weight' => -10,
  ];

  // Used to filter on if a changeset has been in a particular
  // state before.
  $data['cps_changeset_history']['new_status'] = [
    'title' => t('State History'),
    'help' => t('The state history of the changeset.'),
    'filter' => [
      'handler' => 'cps_handler_filter_status',
    ],
  ];

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function cps_views_data_alter(&$data) {
  // This uses views_data_alter() because most of the Views integration
  // is handled by entity module, but this filter is a bit special.
  $data['cps_changeset']['status_type'] = array(
    'filter' => array(
      'title' => t('Status type'),
      'help' => t('Filter by all statuses of a given type'),
      'real field' => 'status',
      'handler' => 'cps_handler_filter_status_type',
    )
  );

  $data['cps_changeset']['cps_changeset_history']['relationship'] = [
    'title' => 'CPS Changeset History',
    'help' => 'State transition history for the changeset.',
    'base' => 'cps_changeset_history',
    'base field' => 'changeset_id',
    'relationship field' => 'changeset_id',
    'handler' => 'views_handler_relationship',
    'entity type' => 'cps_changeset',
  ];

  // Replace entity API created handler with custom one that can reduce to just
  // a set of types.
  $data['cps_changeset']['status']['filter']['handler'] = 'cps_handler_filter_status';

  $types = cps_get_supported();
  $info = entity_get_info();
  foreach ($types as $type) {
    $entity_info = $info[$type];
    $table = $entity_info['base table'];

    $data[$table]['cps_changeset'] = array(
      'title' => t('CPS Site Version'),
      'help' => t('Relates the @label entity to the CPS changeset table through the CPS site version.', array('@label' => $entity_info['label'])),
      'relationship' => array(
        'handler' => 'cps_handler_relationship_changeset',
        'label' => t('cps'),
        'base' => 'cps_changeset',
        'entity type' => $type,
      ),
    );
  }
}
