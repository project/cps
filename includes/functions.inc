<?php

/**
 * @file
 * Core CPS functions.
 */

/**
 * This file exists so low level functions like getting and setting a changeset
 * can be used outside of the module file being loaded. Currently, the main use
 * case for this is the cps_path sub module. the cps_path.inc override for
 * core's path.inc file uses this as it can be loaded and used very early during
 * a request, where loading the module file itself could have side affects.
 */

// -----------------------------------------------------------------------
// Public API

/**
 * Fetch the currently in use changeset.
 *
 * @param $test
 *   If TRUE, test to ensure the changeset is valid.
 *
 * @return string
 *   The current changeset id.
 */
function cps_get_current_changeset($test = FALSE) {

  if (isset($GLOBALS['cps_override_changeset'])) {
    return $GLOBALS['cps_override_changeset'];
  }
  if (!function_exists('user_access')) {
    drupal_load('module', 'user');
  }

  if (!drupal_is_cli() && !cps_can_access_changesets()) {
    return CPS_PUBLISHED_CHANGESET;
  }

  // See if a changeset ID was requested via $_GET or $_POST.
  $requested_changeset_id = NULL;
  if (isset($_REQUEST['changeset_id'])) {
    $requested_changeset_id = $_REQUEST['changeset_id'];
  }
  // If no changeset was requested, see if there was an active changeset in the
  // URL of the page that linked to this one, and if so, use it. This handles
  // cases where cps.js could not add the changeset ID to this page's URL
  // before the page was loaded (for example, if this page was loaded by
  // JavaScript via window.location). This check would actually make most of
  // the link-rewriting code in cps.js unnecessary if it weren't for the fact
  // that some browsers (for example Safari) don't set HTTP_REFERER in the case
  // where a link is opened in a new browser tab.
  elseif (!empty($_SERVER['HTTP_REFERER']) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == strtolower($_SERVER['HTTP_HOST'])) {
    $query_string = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY);
    if ($query_string) {
      parse_str($query_string, $query_array);
      if (isset($query_array['changeset_id'])) {
        $requested_changeset_id = $query_array['changeset_id'];
      }
    }
  }

  if (isset($requested_changeset_id)) {
    $changeset_id = $requested_changeset_id;
    // Store the changeset in the session so it will be used on future
    // requests, unless a URL parameter or global variable is provided
    // requesting that the changeset not be stored.
    if (empty($_GET['changeset_temporary_override']) && empty($GLOBALS['cps_ignore_session'])) {
      $_SESSION['changeset_id'] = $changeset_id;
    }
  }
  elseif (isset($_SESSION['changeset_id']) && empty($GLOBALS['cps_ignore_session'])) {
    $changeset_id = $_SESSION['changeset_id'];
  }
  else {
    $changeset_id = CPS_PUBLISHED_CHANGESET;
  }

  // Use a $test flag because there are phases where we do not want to invoke
  // a load.
  if ($test && $changeset_id != CPS_PUBLISHED_CHANGESET) {
    $changeset = cps_changeset_load($changeset_id);

    if (!$changeset) {
      // If no changeset was loaded, default to published.
      $changeset_id = CPS_PUBLISHED_CHANGESET;
    }
  }

  if ($changeset_id != CPS_PUBLISHED_CHANGESET) {
    // Record that we have touched this changeset.
    $_SESSION['touched changesets'][$changeset_id] = REQUEST_TIME;
  }

  return $changeset_id;
}

/**
 * Set the current changeset.
 *
 * @param string $changeset_id
 *   The changeset id.
 */
function cps_set_current_changeset($changeset_id) {
  if (isset($changeset_id)) {
    $_SESSION['changeset_id'] = $_GET['changeset_id'] = $_REQUEST['changeset_id'] = $changeset_id;
  }
  elseif (isset($_SESSION['changeset_id'])) {
    unset($_SESSION['changeset_id'], $_GET['changeset_id'], $_REQUEST['changeset_id']);
  }
}
