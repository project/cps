<?php

/**
 * @file
 * CPS constants.
 */

/**
 * This file exists so CPS constants can be used outside of the module file
 * being loaded. Currently, the main use case for this is the cps_path sub
 * module. the cps_path.inc override for core's path.inc file uses this as it
 * can be loaded and used very early during a request, where loading the module
 * file itself could have side affects.
 */

define('CPS_PUBLISHED_CHANGESET', 'published');
define('CPS_INSTALLED_CHANGESET', 'installed');
define('CPS_LIVE_STATUS', 'live');
define('CPS_ARCHIVED_STATUS', 'archived');
define('CPS_PUBLISHING_STATUS', 'publishing');
define('CPS_UNPUBLISHING_STATUS', 'unpublishing');
define('CPS_UNPUBLISHED_STATUS', 'unpublished');
define('CPS_STATUS_URI', 'public://cps_process_status.json');
