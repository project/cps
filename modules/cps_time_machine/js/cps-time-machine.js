(function ($) {
  Drupal.behaviors.cpsTimeMachine = {
    attach: function (context, settings) {
      let $element = $('form .cps-time-machine-when', context);
      let $form = $($element).closest('form');
      let $submit = $('#edit-submit', $form);

      $element.change(function() {
        // Tell the user what's happening because this auto-submit isn't obvious.
        $form.hide().before('<div class="throbber"></div><div class="message">' + Drupal.t('Loading - please wait...') + '</div>');

        // Let the window finish closing before submitting.
        setTimeout(function() {
          $submit.click();
        }, 200);
      });
      $submit.hide();
    }
  };
})(jQuery);
