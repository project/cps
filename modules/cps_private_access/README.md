# CPS Private Access

This module enables private site versions, which can only be accessed by the creator of the site version, and optionally by other roles.


## Installation

Enable the module.
Give the 'mark changesets as private' to the roles which are allowed to set a site version as private.


## Usage

A user who has the 'mark changesets as private' permission will see a checkbox to mark a changeset as private, and setting that checkbox will display the user roles who can also access the site version.

Once the private site version is set up, only the creator (and eventually the chosen roles) will be able to access the site version, and any entity edited in this site version will be visible only by the users who can access the site version. The entities should also not appear in the administration views.

