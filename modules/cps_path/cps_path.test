<?php

/**
 * @file
 * Tests for path.inc.
 */

/**
 * Base class for CPS Path web tests.
 */
abstract class CpsPathWebTestCase extends DrupalWebTestCase {

  public function setUp() {
    $modules = func_get_args();
    if (isset($modules[0]) && is_array($modules[0])) {
      $modules = $modules[0];
    }

    // Always add cps_path.
    array_unshift($modules, 'cps_path');

    // Set up the database and testing environment.
    parent::setUp($modules);

    // Set the path_inc variable.
    variable_set('path_inc', drupal_get_path('module', 'cps_path') . '/includes/cps_path.inc');

    // Refresh our static variables from the database.
    $this->refreshVariables();
  }

  /**
   * Helper method to create a changeset for tests.
   *
   * @param \stdClass $account
   *   The account to create the chnangeset for. If empty, 0 will be used.
   *
   * @return \CPSChangeset
   */
  protected function createChangeset($account = NULL) {
    $values = array(
      'name' => $this->randomName(),
      'uid' => isset($account) ? $account->uid : 0,
    );
    $changeset = cps_changeset_create($values);
    $changeset->save();

    return $changeset;
  }

}

/**
 * Unit tests for the drupal_match_path() function in path.inc.
 *
 * @see drupal_match_path().
 */
class CpsPathDrupalMatchPathTestCase extends CpsPathWebTestCase {
  protected $front;

  public static function getInfo() {
    return array(
      'name' => 'CPS Path Drupal match path',
      'description' => 'Tests the drupal_match_path() function to make sure it works properly.',
      'group' => 'CPS Path',
    );
  }

  public function setUp() {
    parent::setUp();

    // Set up a random site front page to test the '<front>' placeholder.
    $this->front = $this->randomName();
    variable_set('site_frontpage', $this->front);

    // Refresh our static variables from the database.
    $this->refreshVariables();
  }

  /**
   * Run through our test cases, making sure each one works as expected.
   */
  public function testDrupalMatchPath() {
    // Set up our test cases.
    $tests = $this->drupalMatchPathTests();
    foreach ($tests as $patterns => $cases) {
      foreach ($cases as $path => $expected_result) {
        $actual_result = drupal_match_path($path, $patterns);
        $this->assertIdentical($actual_result, $expected_result, format_string('Tried matching the path <code>@path</code> to the pattern <pre>@patterns</pre> - expected @expected, got @actual.', array('@path' => $path, '@patterns' => $patterns, '@expected' => var_export($expected_result, TRUE), '@actual' => var_export($actual_result, TRUE))));
      }
    }
  }

  /**
   * Helper function for testDrupalMatchPath(): set up an array of test cases.
   *
   * @return
   *   An array of test cases to cycle through.
   */
  private function drupalMatchPathTests() {
    return array(
      // Single absolute paths.
      'blog/1' => array(
        'blog/1' => TRUE,
        'blog/2' => FALSE,
        'test' => FALSE,
      ),
      // Single paths with wildcards.
      'blog/*' => array(
        'blog/1' => TRUE,
        'blog/2' => TRUE,
        'blog/3/edit' => TRUE,
        'blog/' => TRUE,
        'blog' => FALSE,
        'test' => FALSE,
      ),
      // Single paths with multiple wildcards.
      'node/*/revisions/*' => array(
        'node/1/revisions/3' => TRUE,
        'node/345/revisions/test' => TRUE,
        'node/23/edit' => FALSE,
        'test' => FALSE,
      ),
      // Single paths with '<front>'.
      '<front>' => array(
        $this->front => TRUE,
        "$this->front/" => FALSE,
        "$this->front/edit" => FALSE,
        'node' => FALSE,
        '' => FALSE,
      ),
      // Paths with both '<front>' and wildcards (should not work).
      '<front>/*' => array(
        $this->front => FALSE,
        "$this->front/" => FALSE,
        "$this->front/edit" => FALSE,
        'node/12' => FALSE,
        '' => FALSE,
      ),
      // Multiple paths with the \n delimiter.
      "node/*\nnode/*/edit" => array(
        'node/1' => TRUE,
        'node/view' => TRUE,
        'node/32/edit' => TRUE,
        'node/delete/edit' => TRUE,
        'node/50/delete' => TRUE,
        'test/example' => FALSE,
      ),
      // Multiple paths with the \r delimiter.
      "user/*\rblog/*" => array(
        'user/1' => TRUE,
        'blog/1' => TRUE,
        'user/1/blog/1' => TRUE,
        'user/blog' => TRUE,
        'test/example' => FALSE,
        'user' => FALSE,
        'blog' => FALSE,
      ),
      // Multiple paths with the \r\n delimiter.
      "test\r\n<front>" => array(
        'test' => TRUE,
        $this->front => TRUE,
        'example' => FALSE,
      ),
      // Test existing regular expressions (should be escaped).
      '[^/]+?/[0-9]' => array(
        'test/1' => FALSE,
        '[^/]+?/[0-9]' => TRUE,
      ),
    );
  }
}

/**
 * Tests hook_url_alter functions.
 */
class CpsPathUrlAlterFunctionalTest extends CpsPathWebTestCase {

  public static function getInfo() {
    return array(
      'name' => t('CPS Path URL altering'),
      'description' => t('Tests hook_url_inbound_alter() and hook_url_outbound_alter().'),
      'group' => t('CPS Path'),
    );
  }

  public function setUp() {
    parent::setUp('forum', 'url_alter_test');
  }

  /**
   * Test that URL altering works and that it occurs in the correct order.
   */
  public function testUrlAlter() {
    $account = $this->drupalCreateUser(array('administer url aliases'));
    $this->drupalLogin($account);

    $uid = $account->uid;
    $name = $account->name;

    // Test a single altered path.
    $this->assertUrlInboundAlter("user/$name", "user/$uid");
    $this->assertUrlOutboundAlter("user/$uid", "user/$name");

    // Test that a path always uses its alias.
    $path = array('source' => "user/$uid/test1", 'alias' => 'alias/test1');
    path_save($path);
    $this->assertUrlInboundAlter('alias/test1', "user/$uid/test1");
    $this->assertUrlOutboundAlter("user/$uid/test1", 'alias/test1');

    // Test that alias source paths are normalized in the interface.
    $edit = array('source' => "user/$name/edit", 'alias' => 'alias/test2');
    $this->drupalPost('admin/config/search/path/add', $edit, t('Save'));
    $this->assertText(t('The alias has been saved.'));

    // Test that a path always uses its alias.
    $this->assertUrlInboundAlter('alias/test2', "user/$uid/edit");
    $this->assertUrlOutboundAlter("user/$uid/edit", 'alias/test2');

    // Test a non-existent user is not altered.
    $uid++;
    $this->assertUrlInboundAlter("user/$uid", "user/$uid");
    $this->assertUrlOutboundAlter("user/$uid", "user/$uid");

    // Test that 'forum' is altered to 'community' correctly, both at the root
    // level and for a specific existing forum.
    $this->assertUrlInboundAlter('community', 'forum');
    $this->assertUrlOutboundAlter('forum', 'community');
    $forum_vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE module = 'forum'")->fetchField();
    $tid = db_insert('taxonomy_term_data')
      ->fields(array(
        'name' => $this->randomName(),
        'vid' => $forum_vid,
      ))
      ->execute();
    $this->assertUrlInboundAlter("community/$tid", "forum/$tid");
    $this->assertUrlOutboundAlter("forum/$tid", "community/$tid");
  }

  /**
   * Test current_path() and request_path().
   */
  public function testCurrentUrlRequestedPath() {
    $this->drupalGet('url-alter-test/bar');
    $this->assertRaw('request_path=url-alter-test/bar', 'request_path() returns the requested path.');
    $this->assertRaw('current_path=url-alter-test/foo', 'current_path() returns the internal path.');
  }

  /**
   * Tests that $_GET['q'] is initialized when the request path is empty.
   */
  public function testGetQInitialized() {
    $this->drupalGet('');
    $this->assertText("\$_GET['q'] is non-empty with an empty request path.", "\$_GET['q'] is initialized with an empty request path.");
  }

  /**
   * Assert that an outbound path is altered to an expected value.
   *
   * @param $original
   *   A string with the original path that is run through url().
   * @param $final
   *   A string with the expected result after url().
   * @return
   *   TRUE if $original was correctly altered to $final, FALSE otherwise.
   */
  protected function assertUrlOutboundAlter($original, $final) {
    // Test outbound altering.
    $result = url($original);
    $base_path = base_path() . (variable_get('clean_url', '0') ? '' : '?q=');
    $result = substr($result, strlen($base_path));
    $this->assertIdentical($result, $final, format_string('Altered outbound URL %original, expected %final, and got %result.', array('%original' => $original, '%final' => $final, '%result' => $result)));
  }

  /**
   * Assert that a inbound path is altered to an expected value.
   *
   * @param $original
   *   A string with the aliased or un-normal path that is run through
   *   drupal_get_normal_path().
   * @param $final
   *   A string with the expected result after url().
   * @return
   *   TRUE if $original was correctly altered to $final, FALSE otherwise.
   */
  protected function assertUrlInboundAlter($original, $final) {
    // Test inbound altering.
    $result = drupal_get_normal_path($original);
    $this->assertIdentical($result, $final, format_string('Altered inbound URL %original, expected %final, and got %result.', array('%original' => $original, '%final' => $final, '%result' => $result)));
  }
}

/**
 * Unit test for drupal_lookup_path().
 */
class CpsPathLookupTest extends CpsPathWebTestCase {

  public static function getInfo() {
    return array(
      'name' => t('CPS Path lookup'),
      'description' => t('Tests that drupal_lookup_path() returns correct paths.'),
      'group' => t('CPS Path'),
    );
  }

  /**
   * Test that drupal_lookup_path() returns the correct path.
   */
  public function testDrupalLookupPath() {
    $account = $this->drupalCreateUser();
    $uid = $account->uid;
    $name = $account->name;

    // Test the situation where the source is the same for multiple aliases.
    // Start with a language-neutral alias, which we will override.
    $path = array(
      'source' => "user/$uid",
      'alias' => 'foo',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), $path['alias']);
    $this->assertEqual(drupal_lookup_path('source', $path['alias']), $path['source']);

    // Create a language specific alias for the default language (English).
    $path = array(
      'source' => "user/$uid",
      'alias' => "users/$name",
      'language' => 'en',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), $path['alias']);
    $this->assertEqual(drupal_lookup_path('source', $path['alias']), $path['source']);

    // Create a language-neutral alias for the same path, again.
    $path = array(
      'source' => "user/$uid",
      'alias' => 'bar',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), "users/$name");

    // Create a language-specific (xx-lolspeak) alias for the same path.
    $path = array(
      'source' => "user/$uid",
      'alias' => 'LOL',
      'language' => 'xx-lolspeak',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), "users/$name");
    // The LOLspeak alias should be returned if we really want LOLspeak.
    $this->assertEqual(drupal_lookup_path('alias', $path['source'], 'xx-lolspeak'), 'LOL');

    // Create a new alias for this path in English, which should override the
    // previous alias for "user/$uid".
    $path = array(
      'source' => "user/$uid",
      'alias' => 'users/my-new-path',
      'language' => 'en',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), $path['alias'], 'Recently created English alias returned.');
    $this->assertEqual(drupal_lookup_path('source', $path['alias']), $path['source'], 'Recently created English source returned.');

    // Remove the English aliases, which should cause a fallback to the most
    // recently created language-neutral alias, 'bar'.
    db_delete('url_alias')
      ->condition('language', 'en')
      ->execute();
    drupal_clear_path_cache();
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), 'bar', 'Path lookup falls back to recently created language-neutral alias.');

    // Test the situation where the alias and language are the same, but
    // the source differs. The newer alias record should be returned.
    $account2 = $this->drupalCreateUser();
    $path = array(
      'source' => 'user/' . $account2->uid,
      'alias' => 'bar',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('source', $path['alias']), $path['source'], 'Newer alias record is returned when comparing two LANGUAGE_NONE paths with the same alias.');
  }

  /**
   * Test path lookups when in a changeset.
   */
  public function testDrupalLookupPathChangeset() {
    $account = $this->drupalCreateUser(['administer changesets']);
    $changeset = $this->createChangeset($account);

    cps_set_current_changeset($changeset->changeset_id);

    $uid = $account->uid;
    $name = $account->name;

    // Test the situation where the source is the same for multiple aliases.
    // Start with a language-neutral alias, which we will override.
    $path = array(
      'source' => "user/$uid",
      'alias' => 'foo',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), $path['alias']);
    $this->assertEqual(drupal_lookup_path('source', $path['alias']), $path['source']);

    // Create a language specific alias for the default language (English).
    $path = array(
      'source' => "user/$uid",
      'alias' => "users/$name",
      'language' => 'en',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), $path['alias']);
    $this->assertEqual(drupal_lookup_path('source', $path['alias']), $path['source']);

    // Create a language-neutral alias for the same path, again.
    $path = array(
      'source' => "user/$uid",
      'alias' => 'bar',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), "users/$name");

    // Create a language-specific (xx-lolspeak) alias for the same path.
    $path = array(
      'source' => "user/$uid",
      'alias' => 'LOL',
      'language' => 'xx-lolspeak',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), "users/$name");
    // The LOLspeak alias should be returned if we really want LOLspeak.
    $this->assertEqual(drupal_lookup_path('alias', $path['source'], 'xx-lolspeak'), 'LOL');

    // Create a new alias for this path in English, which should override the
    // previous alias for "user/$uid".
    $path = array(
      'source' => "user/$uid",
      'alias' => 'users/my-new-path',
      'language' => 'en',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), $path['alias'], 'Recently created English alias returned.');
    $this->assertEqual(drupal_lookup_path('source', $path['alias']), $path['source'], 'Recently created English source returned.');

    // Remove the English aliases, which should cause a fallback to the most
    // recently created language-neutral alias, 'bar'.
    db_delete('cps_url_alias')
      ->condition('language', 'en')
      ->execute();
    drupal_clear_path_cache();
    $this->assertEqual(drupal_lookup_path('alias', $path['source']), 'bar', 'Path lookup falls back to recently created language-neutral alias.');

    // Test the situation where the alias and language are the same, but
    // the source differs. The newer alias record should be returned.
    $account2 = $this->drupalCreateUser();
    $path = array(
      'source' => 'user/' . $account2->uid,
      'alias' => 'bar',
    );
    path_save($path);
    $this->assertEqual(drupal_lookup_path('source', $path['alias']), $path['source'], 'Newer alias record is returned when comparing two LANGUAGE_NONE paths with the same alias.');

    cps_set_current_changeset(NULL);
  }

  /**
   * Test path lookups to published aliases when in a changeset.
   */
  public function testDrupalLookupPathChangesetFallback() {
    $account = $this->drupalCreateUser(['administer changesets']);
    $changeset = $this->createChangeset($account);

    cps_set_current_changeset($changeset->changeset_id);

    $uid = $account->uid;
    $name = $account->name;

    // Test the situation where the source is the same for multiple aliases.
    // Start with a language-neutral alias, which we will override.
    $path_a = array(
      'source' => "user/$uid",
      'alias' => 'foo',
    );
    path_save($path_a);
    $this->assertEqual(drupal_lookup_path('alias', $path_a['source']), $path_a['alias']);
    $this->assertEqual(drupal_lookup_path('source', $path_a['alias']), $path_a['source']);

    path_delete($path_a);

    $this->assertFalse(drupal_lookup_path('alias', $path_a['source']));
    $this->assertFalse(drupal_lookup_path('source', $path_a['alias']));

    // Add a path alias to the published changeset (url_alias).
    cps_set_current_changeset(NULL);

    $path_b = array(
      'source' => "user/$uid",
      'alias' => "users/$name",
    );
    path_save($path_b);

    // Switch back to the test changeset.
    cps_set_current_changeset($changeset->changeset_id);

    // Path lookup should now match the published as a fallback.
    $this->assertEqual(drupal_lookup_path('alias', $path_b['source']), $path_b['alias']);
    $this->assertEqual(drupal_lookup_path('source', $path_b['alias']), $path_b['source']);

    // Add back path A. This should now override the fallback.
    path_save($path_a);
    $this->assertEqual(drupal_lookup_path('alias', $path_a['source']), $path_a['alias']);
    $this->assertEqual(drupal_lookup_path('source', $path_a['alias']), $path_a['source']);

    // Delete the path. Fallback should be used again.
    path_delete($path_a);

    // Path lookup should now match the published as a fallback.
    $this->assertEqual(drupal_lookup_path('alias', $path_b['source']), $path_b['alias']);
    $this->assertEqual(drupal_lookup_path('source', $path_b['alias']), $path_b['source']);

    cps_set_current_changeset(NULL);
  }

}

/**
 * Tests the path_save() function.
 */
class CpsPathSaveTest extends CpsPathWebTestCase {

  public static function getInfo() {
    return array(
      'name' => t('CPS Path save'),
      'description' => t('Tests that path_save() exposes the previous alias value.'),
      'group' => t('CPS Path'),
    );
  }

  public function setUp() {
    // Enable a helper module that implements hook_path_update().
    parent::setUp('path_test');

    path_test_reset();
  }

  /**
   * Tests that path_save() makes the original path available to modules.
   */
  public function testDrupalSaveOriginalPath() {
    $account = $this->drupalCreateUser();
    $uid = $account->uid;

    // Create a language-neutral alias.
    $path = array(
      'source' => "user/$uid",
      'alias' => 'foo',
    );
    $path_original = $path;
    path_save($path);

    // Alter the path.
    $path['alias'] = 'bar';
    path_save($path);

    // Test to see if the original alias is available to modules during
    // hook_path_update().
    $results = variable_get('path_test_results', array());
    $this->assertIdentical($results['hook_path_update']['original']['alias'], $path_original['alias'], 'Old path alias available to modules during hook_path_update.');
    $this->assertIdentical($results['hook_path_update']['original']['source'], $path_original['source'], 'Old path alias available to modules during hook_path_update.');
  }

  /**
   * Tests that path_save() makes the original path available to modules.
   */
  public function testDrupalSaveOriginalPathChangeset() {
    $account = $this->drupalCreateUser(['administer changesets']);
    $changeset = $this->createChangeset($account);

    cps_set_current_changeset($changeset->changeset_id);

    $account = $this->drupalCreateUser();
    $uid = $account->uid;

    // Create a language-neutral alias.
    $path = array(
      'source' => "user/$uid",
      'alias' => 'foo',
    );
    $path_original = $path;
    path_save($path);

    // Alter the path.
    $path['alias'] = 'bar';
    path_save($path);

    // Test to see if the original alias is available to modules during
    // hook_path_update().
    $results = variable_get('path_test_results', array());
    $this->assertIdentical($results['hook_path_update']['original']['alias'], $path_original['alias'], 'Old path alias available to modules during hook_path_update.');
    $this->assertIdentical($results['hook_path_update']['original']['source'], $path_original['source'], 'Old path alias available to modules during hook_path_update.');

    cps_set_current_changeset(NULL);
  }

}
