<?php

/**
 * CPS Path Module.
 */

/**
 * Implements hook_cps_changeset_published().
 */
function cps_path_cps_changeset_published($changeset, $type) {
  $transaction = db_transaction();

  try {
    switch ($type) {
      case CPS_ARCHIVED_STATUS:
        // Copy all path aliases from the current changeset to the published
        // changeset.
        $changeset_paths = db_select('cps_url_alias', 'cua')
          ->fields('cua')
          ->condition('changeset_id', $changeset->changeset_id)
          ->execute()
          ->fetchAllAssoc('pid', PDO::FETCH_ASSOC);

        // Collect all source paths from the current changeset path records.
        $changeset_source_paths = array();
        foreach ($changeset_paths as $changeset_path) {
          $changeset_source_paths[] = $changeset_path['source'];
        }

        // If there are no paths from this changeset, there is nothing else to
        // do.
        if (empty($changeset_source_paths)) {
          return;
        }

        // Fetch all currently published paths so previous_alias can be written
        // to the changeset record.
        $published_changeset_paths = db_select('url_alias', 'ua')
          ->fields('ua')
          ->condition('source', $changeset_source_paths)
          ->execute()
          ->fetchAllAssoc('source', PDO::FETCH_ASSOC);

        // Change to the just published changeset to write the previous aliases.
        cps_override_changeset($changeset->changeset_id);

        foreach ($changeset_paths as $changeset_path) {
          // Check if there is a published alias by matching the source, if so,
          // add it as a previous alias and write the path back.
          if (isset($published_changeset_paths[$changeset_path['source']])) {
            $changeset_path['previous_alias'] = $published_changeset_paths[$changeset_path['source']]['alias'];
            path_save($changeset_path);
          }
        }

        cps_override_changeset(CPS_PUBLISHED_CHANGESET);

        // Delete published aliases matching the changeset paths about to be
        // written from the url_alias table.
        db_delete('url_alias')
          ->condition('source', $changeset_source_paths)
          ->execute();

        // Create new versions of the changeset paths.
        foreach ($changeset_paths as $changeset_path) {
          if (empty($changeset_path['alias'])) {
            // Empty aliases represent a deletion.
            continue;
          }
          // Remove pid so we get an insert and update to published changeset ID.
          unset($changeset_path['pid']);
          path_save($changeset_path);
        }

        cps_override_changeset(NULL);

        break;
      case CPS_UNPUBLISHED_STATUS:
        // Remove all published paths from the current changeset being
        // unpublished.
        $changeset_paths = db_select('cps_url_alias', 'cua')
          ->fields('cua')
          ->condition('changeset_id', $changeset->changeset_id)
          ->execute()
          ->fetchAllAssoc('pid', PDO::FETCH_ASSOC);

        $changeset_source_paths = array();
        foreach ($changeset_paths as $changeset_path) {
          $changeset_source_paths[] = $changeset_path['source'];
        }

        // If there are no paths from this changeset, there is nothing else to
        // do.
        if (empty($changeset_source_paths)) {
          return;
        }

        // Remove the currently published paths for the changeset being
        // unpublished from the url_alias.
        db_delete('url_alias')
          ->condition('source', $changeset_source_paths)
          ->execute();

        // Change to the just published changeset to write back the previous
        // aliases.
        cps_override_changeset(CPS_PUBLISHED_CHANGESET);

        // Write the aliases again, but use the previous_alias from changeset
        // publishing.
        foreach ($changeset_paths as $changeset_path) {
          // Copy the previous alias to the alias.
          $changeset_path['alias'] = $changeset_path['previous_alias'];
          // Remove pid so we get an insert and update to published changeset ID.
          unset($changeset_path['pid'], $changeset_path['previous_alias']);
          path_save($changeset_path);
        }

        cps_override_changeset(NULL);

        break;
    }
  }
  catch (\Exception $e) {
    $transaction->rollback();
    throw $e;
  }
}

/**
 * Implements hook_cps_remove_changeset().
 */
function cps_path_cps_remove_changeset(CPSChangeset $changeset, $entity_type, $entity) {
  cps_path_remove_cps_url_alias($changeset->changeset_id, $entity_type, $entity);
}

/**
 * Implements hook_entity_delete().
 */
function cps_path_entity_delete($entity, $entity_type) {
  $changeset_id = cps_get_current_changeset();
  if ($changeset_id != CPS_PUBLISHED_CHANGESET) {
    cps_path_remove_cps_url_alias($changeset_id, $entity_type, $entity);
  }
}

/**
 * Implements hook_cps_move_changeset().
 */
function cps_path_cps_move_changeset($changeset_from, $changeset_to, $entity_type, $entity, $type) {
  // When copying or moving an entity, also copy/move its alias.
  $uri = entity_uri($entity_type, $entity);
  $path = db_select('cps_url_alias', 'cua')
    ->fields('cua')
    ->condition('changeset_id', $changeset_from->changeset_id)
    ->condition('source', $uri['path'])
    ->execute()
    ->fetchAssoc();

  if ($type == 'move') {
    cps_path_remove_cps_url_alias($changeset_from->changeset_id, $entity_type, $entity);
  }
  else {
    // On copy, create a new record.
    unset($path['pid']);
  }

  $path['changeset_id'] = $changeset_to->changeset_id;
  drupal_write_record('cps_url_alias', $path);
}

/**
 * Removes a url alias from a changeset.
 *
 * @param string $changeset_id
 *   The ID of the changeset.
 * @param $entity_type
 *   The type of the entity.
 * @param $entity
 *   The entity.
 *
 * @return void
 */
function cps_path_remove_cps_url_alias($changeset_id, $entity_type, $entity) {
  $uri = entity_uri($entity_type, $entity);
  if (!$uri) {
    // Skip entities that have no URI.
    return;
  }
  db_delete('cps_url_alias')
    ->condition('changeset_id', $changeset_id)
    ->condition('source', $uri['path'])
    ->execute();
}
