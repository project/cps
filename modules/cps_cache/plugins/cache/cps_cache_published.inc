<?php

/**
 * @file
 * Provides a caching option for the published changeset.
 */

// Plugin definition
$plugin = array(
  'title' => t('CPS Published cache'),
  'description' => t('Simple caching with no time limit. Caches get cleared when a changeset is published.'),
  'cache get' => 'cps_cache_published_cache_get_cache',
  'cache set' => 'cps_cache_published_cache_set_cache',
  'cache clear' => 'cps_cache_published_cache_clear_cache',
  'defaults' => array(),
);

/**
 * Get cached content.
 */
function cps_cache_published_cache_get_cache($conf, $display, $args, $contexts, $pane = NULL) {
  // Don't cache inside active changesets.
  if (cps_is_current_changeset_active()) {
    return FALSE;
  }

  $cid = cps_cache_published_cache_get_id($conf, $display, $pane);
  $cache = cache_get($cid, CPS_CACHE_PUBLISHED_CACHE_BIN);

  if (!$cache) {
    return FALSE;
  }

  return $cache->data;
}

/**
 * Set cached content.
 */
function cps_cache_published_cache_set_cache($conf, $content, $display, $args, $contexts, $pane = NULL) {
  // Don't cache inside active changesets.
  if (cps_is_current_changeset_active()) {
    return FALSE;
  }

  $cid = cps_cache_published_cache_get_id($conf, $display, $pane);
  cache_set($cid, $content, CPS_CACHE_PUBLISHED_CACHE_BIN);
}

/**
 * Clear cached content.
 *
 * Cache clears are always for an entire display, regardless of arguments.
 */
function cps_cache_published_cache_clear_cache($display) {
  // Don't need to clear inside active changesets.
  if (cps_is_current_changeset_active()) {
    return FALSE;
  }

  $cid = CPS_CACHE_PUBLISHED_CACHE_CID_BASE;

  // This is used in case this is an in-code display, which means did will be something like 'new-1'.
  if (isset($display->owner) && isset($display->owner->id)) {
    $cid .= ':' . $display->owner->id;
  }

  $cid .= ':' . $display->did;

  cache_clear_all($cid, CPS_CACHE_PUBLISHED_CACHE_BIN, TRUE);
}

/**
 * Figure out an id for our cache based upon input and settings.
 */
function cps_cache_published_cache_get_id($conf, $display, $pane) {
  $cid_parts = array(CPS_CACHE_PUBLISHED_CACHE_CID_BASE);

  // If the panel is stored in the database it'll have a numeric did value.
  if (is_numeric($display->did)) {
    $cid_parts[] = $display->did;
  }
  // Exported panels won't have a numeric did but may have a usable cache_key.
  elseif (!empty($display->cache_key)) {
    $cid_parts[] = str_replace('panel_context:', '', $display->cache_key);
  }
  // Alternatively use the css_id.
  elseif (!empty($display->css_id)) {
    $cid_parts[] = $display->css_id;
  }
  // Failover to just appending the did, which may be the completely unusable
  // string 'new'.
  else {
    $cid_parts[] = $display->did;
  }

  if ($pane) {
    $cid_parts[] = $pane->pid;

    if(!empty($pane->configuration['use_pager']) && !empty($_GET['page'])) {
      $cid_parts[] = 'p' . check_plain($_GET['page']);
    }
  }

  if (user_access('view pane admin links')) {
    $cid_parts[] = 'admin';
  }

  if (module_exists('locale')) {
    global $language;
    $cid_parts[] = $language->language;
  }

  $context = array(
    'type' => 'panels',
    'conf' => $conf,
    'display' => $display,
    'pane' => $pane,
  );

  cps_cache_alter_cid($cid_parts, $context);

  return implode(':', $cid_parts);
}
