<?php


class views_plugin_cache_cps_published extends views_plugin_cache {

  /**
   * What table to store data in.
   */
  var $table = CPS_CACHE_PUBLISHED_CACHE_BIN;

  /**
   * {@inheritdoc}
   */
  public function cache_set($type) {
    if (cps_is_current_changeset_active()) {
      return FALSE;
    }

    parent::cache_set($type);
  }

  /**
   * {@inheritdoc}
   */
  public function cache_get($type) {
    if (cps_is_current_changeset_active()) {
      return FALSE;
    }

    return parent::cache_get($type);
  }

  /**
   * {@inheritdoc}
   */
  public function get_cache_key($key_data = array()) {
    $key = parent::get_cache_key($key_data);

    $cid_parts = [$key];

    $context = array(
      'type' => 'views',
      'view' => $this->view,
    );

    cps_cache_alter_cid($cid_parts, $context);

    return implode(':', $cid_parts);
  }

  
}
