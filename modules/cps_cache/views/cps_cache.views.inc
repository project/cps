<?php

/**
 * Implements hook_views_plugins().
 */
function cps_cache_views_plugins() {
  $plugins = array();

  $plugins['cache'] = array(
    'cps_published' => array(
      'title' => t('CPS Published'),
      'help' => t('Simple caching with no time limit. Caches get cleared when a changeset is published.'),
      'handler' => 'views_plugin_cache_cps_published',
      'uses options' => FALSE,
    ),
  );

  return $plugins;
}
