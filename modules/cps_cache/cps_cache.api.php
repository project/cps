<?php

/**
 * @file
 * CPS Panels hooks
 */

/**
 * Alters the CPS Panels cache ID.
 *
 * @param array $cid
 *   The cid parts to alter.
 * @param array $context
 *   An array of context. This array should always contain a 'type' key
 *   (E.g. 'panels'). The other keys present will depend on the type. If this is
 *   altering something for panels there with be 'conf', 'display', and 'pane'
 *   keys. If this is views, there will just be a 'view' key.
 */
function hook_cps_cache_published_cache_cid_alter(array &$cid_parts, array $context) {

}
