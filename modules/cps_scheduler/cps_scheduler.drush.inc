<?php

/**
 * @file
 * CPS Scheduler drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function cps_scheduler_drush_command() {
  $items['cps-scheduler-run'] = array(
    'description' => t('Run scheduled publishing.'),
    'aliases' => array('cpssr'),
  );

  return $items;
}

/**
 * Drush command callback
 */
function drush_cps_scheduler_run() {
  _cps_scheduler_scheduled_publish();
}
