<?php

/**
 * Implements hook_views_data.
 */
function cps_scheduler_views_data() {
  $data['cps_scheduler']['table']['group'] = t('Site version');

  $data['cps_scheduler']['table']['base'] = array(
    'field' => 'changeset_id',
    'title' => 'CPS Scheduler',
    'help' => 'CPS Scheduler.',
    'weight' => -10,
  );

  $data['cps_scheduler']['table']['join'] = array(
    'cps_changeset' => array(
      'left_table' => 'cps_changeset',
      'left_field' => 'changeset_id',
      'field' => 'changeset_id',
      'type' => 'LEFT',
    ),
  );

  $data['cps_scheduler']['publish_on'] = array(
    'title' => 'Publish on',
    'help' => 'The publish on.',
    'field' => array(
      'handler' => 'views_handler_field_date',
    ),
  );

  return $data;
}
