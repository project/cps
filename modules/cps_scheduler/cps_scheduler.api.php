<?php

/**
 * @file
 * Hooks provided by the CPS scheduler module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the errors when checking if a changeset can be scheduled.
 *
 * @param $errors
 *  An array of errors keyed by type
 *
 * @param $entities
 *  The list of tracked entitites
 */
function hook_cps_scheduler_not_allowed_alter(&$errors, $entities) {
  unset($errors['entity_type:taxonomy_term']);
  foreach ($entities as $entity_type => $entities) {
    foreach ($entities as $entity_id) {
      if ($entity_type == 'node' && $entity_id == 1) {
        $errors[$entity_type . ':' . $entity_id] = FALSE;
      }
    }
  }
}

/**
 * Implements hook_cps_scheduler_status_message_alter().
 *
 * @param string $message
 *    The changeset transition status message.
 *
 * @param int $publish_on
 *    Scheduled for timestamp.
 */
function hook_cps_scheduler_status_message_alter(&$message, $publish_on) {

}

/**
 * React when the changeset is scheduled or unscheduled.
 *
 * @param CPSChangeset $changeset
 *   The changeset entity.
 * @param string $type
 *   Either CPS_SCHEDULED_STATUS or CPS_UNSCHEDULED_STATUS.
 */
function hook_cps_scheduler_changeset_scheduled($changeset, $type) {

}

/**
 * @} End of "addtogroup hooks".
 */
