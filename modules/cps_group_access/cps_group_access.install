<?php

/**
 * @file cps_group_access.install
 */

/**
 * Implements hook_schema().
 */
function cps_group_access_schema() {
  $schema['cps_changeset_group'] = [
    'description' => 'Stores groups for changesets.',
    'fields' => [
      'changeset_id' => [
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The id of the changeset.',
      ],
      'eid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The id of the entity the changeset was tagged with.',
      ],
    ],
    'primary key' => ['changeset_id', 'eid'],
  ];

  return $schema;
}

/**
 * Implements hook_install().
 */
function cps_group_access_install() {
  $config = cps_group_access_get_config();

  // Create the target taxonomy only if we're not using another entity.
  if (cps_group_access_is_default_config()) {
    $new_vocab = array(
      'name' => 'CPS groups',
      'machine_name' => 'cps_group_access',
      'description' => 'A list of groups to apply to users and cps entities.',
    );
    if (!taxonomy_vocabulary_machine_name_load($new_vocab['machine_name'])) {
      taxonomy_vocabulary_save((object) $new_vocab);
    }
  }

  // Add field.
  $field = [
    'translatable' => 0,
    'settings' => [
      'target_type' => $config['entity_type'],
      'handler_settings' => [
        'target_bundles' => [
          $config['entity_bundle'] => $config['entity_bundle']
        ]
      ]
    ],
    'field_name' => 'field_cps_group_access',
    'type' => 'entityreference',
    'module' => 'entityreference',
    'active' => 1,
      'locked' => 0,
      'cardinality' => -1,
      'deleted' => 0,
      'bundles' => [
      'user' => [
        '0' => 'user'
      ]
    ]
  ];

  if (!field_read_field($field['field_name'])) {
    field_create_field($field);
  }

  $instance = [
    'label' => 'CPS groups',
    'required' => 0,
    'field_name' => 'field_cps_group_access',
    'entity_type' => 'user',
    'bundle' => 'user',
    'deleted' => 0,
    'widget' => [
      'type' => 'options_buttons',
      'module' => 'options',
      'active' => 1,
    ],
    'settings' => [
      'user_register_form' => 0
    ],
    'display' => [
      'default' => [
        'type' => 'hidden',
      ]
    ],
  ];

  if (!field_read_instance($instance['entity_type'], $instance['field_name'], $instance['bundle'])) {
    field_create_instance($instance);
  }
}

/**
 * Implements hook_uninstall().
 */
function cps_group_access_uninstall() {
  $vocabulary = taxonomy_vocabulary_machine_name_load('cps_group_access');
  if ($vocabulary) {
    taxonomy_vocabulary_delete($vocabulary->vid);
  }

  field_delete_field('field_cps_group_access');
}
