# CPS Group Access

This module enable giving users access to some site versions, based on a group. Access will be granted if a user is tagged with one of the groups the site version is also tagged with.

Note that this module doesn't use fields for CPSChangesets, because they have an alphanumeric id, and both entity API and entityreference assume the id is an integer.


## Installation

By default this module uses a taxonomy, which is used to manage the groups. This is simple but has drawbacks, in particular that you'll need to publish a changeset to make changes live, if you're using taxonomy revisions.

If this is not acceptable for you, you can use another entity, or create a custom entity to map the groups, and configure these variables:
```
$conf['cps_group_access_target'] = [
  'entity_type' => 'node',
  'entity_bundle' => 'article',
];
```

1) Enable the module, this will create a new "CPS groups" taxonomy unless you configure a different entity, and add an entity reference field to it for users.

2) Edit the permissions, and add the "Administer changesets" and "Only access site versions in the same group" permissions to the affected roles. Note that despite the name, "Administer changesets" is not a broad permission (it only allows creating and editing one's own changesets).


## Usage

Add you groups to the CPS Groups taxonomy, or to the entity you're using: if you're using a taxonomy, don't forget to publish the site version.

To tag users and site versions you simply need to edit the affected users and site versions and tag them with the same group:
1) visit the site version admin page and edit a version you want to give tagged access to
2) visit the users admin page and edit a user

