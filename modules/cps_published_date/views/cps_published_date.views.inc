<?php

/**
 * @file
 * Views hooks implemented for the CPS Published Date module.
 */

/**
 * Implements hook_views_data_alter().
 */
function cps_published_date_views_data_alter(&$data) {
  $joins = [];
  $supported_entity_types = cps_get_supported();
  foreach ($data as $name => $views_data) {
    $views_entity_type = $views_data['table']['entity type'] ?? '';
    $views_entity_field = $views_data['table']['base']['field'] ?? '';
    if (empty($views_entity_type) || empty($views_entity_field)) {
      continue;
    }
    if (!in_array($views_entity_type, $supported_entity_types)) {
      continue;
    }
    // Avoid revision tables.
    $revision_suffix = '_revision';
    if (substr($name, -(strlen($revision_suffix))) === $revision_suffix) {
      continue;
    }
    $joins[$views_entity_type] = [
      'left_field' => $views_entity_field,
      'field' => 'entity_id',
      'extra' => CPS_PUBLISHED_DATE_TABLE . ".entity_type = '{$views_entity_type}'",
    ];
  }
  if (!$joins) {
    return;
  }

  $data[CPS_PUBLISHED_DATE_TABLE]['table']['group'] = nt('CPS Published Date');
  $data[CPS_PUBLISHED_DATE_TABLE]['table']['base'] = [
    'field' => 'entity_id',
    'title' => nt('CPS Published Date'),
    'help' => nt('The published date data for entities.'),
  ];
  $data[CPS_PUBLISHED_DATE_TABLE]['table']['join'] = $joins;

  $data[CPS_PUBLISHED_DATE_TABLE]['published_date'] = [
    'title' => nt('Published date'),
    'help' => nt('The Unix timestamp when the entity was published in a CPS changeset.'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_date',
    ],
  ];
}
