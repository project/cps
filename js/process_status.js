(function ($) {
  Drupal.behaviors.cpsProcessStatus = {
    abortProcessJson: false,
    attach: function(context, settings) {
      // Hide the action links while the changeset is being published, since
      // they'll do nothing, and also don't reflect the publishing state.
      $('.action-links').hide();

      var $iframe = $(context).find('#cps-process-placeholder iframe');
      if ($iframe.length) {
        // Begin polling for processing updates associated with the requested
        // changeset and operation.
        var status_url = settings.cps_process_status_url;
        var changeset_id = settings.cps_process_changeset_id;
        var operation = settings.cps_process_operation;
        Drupal.behaviors.cpsProcessStatus.processJson(status_url, changeset_id, operation);
        // When the hidden iframe is fully loaded, check its contents for any
        // additional instructions.
        $iframe.on('load', function() {
          Drupal.behaviors.cpsProcessStatus.processIframeInstructions($(this));
        });
        // The iframe may have completed its initial load before the above
        // handler is attached to it. So call the function directly as well.
        Drupal.behaviors.cpsProcessStatus.processIframeInstructions($iframe);
      }
    },
    processJson: function(status_url, changeset_id, operation) {
      $.getJSON(status_url + '?timestamp=' + Date.now()).done(function(data) {
        // Only display the message if it's associated with the requested
        // operation on the requested changeset.
        if (data != null && data.changeset_id == changeset_id && data.operation == operation) {
          var is_error_message = (data.complete === -1);
          Drupal.behaviors.cpsProcessStatus.displayMessage(data.message, is_error_message);
          if (data.complete !== 1) {
            if (!Drupal.behaviors.cpsProcessStatus.abortProcessJson) {
              setTimeout(function() {
                Drupal.behaviors.cpsProcessStatus.processJson(status_url, changeset_id, operation);
              }, 200);
            }
          }
          else {
            Drupal.behaviors.cpsProcessStatus.redirectToChangesetStatusPage();
          }
        }
        else {
          if (!Drupal.behaviors.cpsProcessStatus.abortProcessJson) {
            setTimeout(function() {
              Drupal.behaviors.cpsProcessStatus.processJson(status_url, changeset_id, operation);
            }, 1000);
          }
        }
      }).fail(function() {
        // There may be a temporary network error, so try again on failure.
        if (!Drupal.behaviors.cpsProcessStatus.abortProcessJson) {
          setTimeout(function() {
            Drupal.behaviors.cpsProcessStatus.processJson(status_url, changeset_id, operation);
          }, 1000);
        }
      });
    },
    processIframeInstructions: function($iframe) {
      // Search for instructions in the iframe content, and quit if there are
      // none. Note that this code works because the iframe and parent page
      // have the same origin.
      var $process_instructions = $iframe.contents().find('#cps-process-instructions');
      if (!$process_instructions.length) {
        return;
      }
      // Display a message if there is one.
      var $message = $process_instructions.find('#cps-process-message');
      if ($message.length) {
        var is_error_message = $message.hasClass('cps-process-error-message');
        Drupal.behaviors.cpsProcessStatus.displayMessage($message.html(), is_error_message);
      }
      // Perform an action if one was requested.
      var $action = $process_instructions.find('#cps-process-action');
      if ($action.length) {
        var action = $action.data('action');
        if (action == 'abort') {
          Drupal.behaviors.cpsProcessStatus.abortProcessJson = true;
        }
        else if (action == 'redirect') {
          Drupal.behaviors.cpsProcessStatus.redirectToChangesetStatusPage();
        }
        else if (action == 'retry') {
          Drupal.behaviors.cpsProcessStatus.reloadIframe($iframe);
        }
      }
    },
    displayMessage: function(message, is_error_message) {
      // Display the message in the correct area of the page.
      var $message_placeholder = $('#cps-process-placeholder');
      var $message_location = $message_placeholder.find('span.cps-process');
      $message_location.html(message);
      if (is_error_message) {
        $message_placeholder.removeClass('status').removeClass('ok').addClass('error');
      }
      else {
        $message_placeholder.removeClass('error').addClass('status').addClass('ok');
      }
    },
    redirectToChangesetStatusPage: function() {
      window.location.href = window.location.href.split('?')[0] + '?changeset_id=published';
    },
    reloadIframe: function ($iframe) {
      setTimeout(function() {
        // To reliably force a reload, the iframe src must change, so append a
        // query parameter for that purpose.
        var src = $iframe.attr('src');
        var timestamp = Date.now();
        if (src.indexOf('cps_timestamp') !== -1) {
          src = src.replace(/([\?&])(cps_timestamp=)[^&#]*/, '$1$2' + timestamp);
        }
        else {
          var separator = (src.indexOf('?') !== -1) ? '&' : '?';
          src += separator + 'cps_timestamp=' + timestamp;
        }
        $iframe.attr('src', src);
      }, 1000);
    }
  };
})(jQuery);
