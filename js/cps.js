(function ($) {
  /**
   * Force a refresh when the select box is changed.
   */

  Drupal.CPS = {};

  // Helper function to add/update query parameter. There are libraries to
  // generalize URL handling, but seem too heavy weight for what we need.
  Drupal.CPS.getChangesetUrl = function(url, changeset_id, args) {
    var defaults = {
      // Set to false to leave any existing changeset ID in the passed-in URL,
      // rather than replacing it with the new changeset ID. This is important
      // for link-rewriting code, because some links (for example, on the Site
      // Versions page) deliberately have a specific changeset ID in the URL
      // already.
      replace_existing: true,
      // Set to true to add a parameter to the URL that (if the URL is
      // followed) will tell the server not to write the changeset to the
      // user's session.
      temporary_override: false
    };
    args = $.extend({}, defaults, args);

    // Replace or ignore the value if changeset_id is already in the URL.
    if (url.indexOf('changeset_id') !== -1) {
      if (args.replace_existing) {
        url = url.replace(/([\?&])(changeset_id=)[^&#]*/, '$1$2' + changeset_id);
      }
    }
    else {
      var separator = (url.indexOf('?') !== -1) ? '&' : '?';

      // Ensure that hash comes after query string.
      if (url.indexOf('#') !== -1) {
        var split = url.split('#');
        split[0] += separator + 'changeset_id=' + changeset_id;
        url = split.join('#');
      }
      else {
        url += separator + 'changeset_id=' + changeset_id;
      }
    }

    // Tell the server to only override the changeset temporarily for the
    // current request.
    if (args.temporary_override && url.indexOf('changeset_temporary_override=1') === -1) {
      url += '&changeset_temporary_override=1';
    }

    return url;
  };

  Drupal.behaviors.cps = {
    ajax_overridden: false,

    attach: function (context, settings) {
      // Add the active changeset_id to the query parameter.
      if ("cps" in settings && "changeset_id" in settings.cps) {
        var url = Drupal.CPS.getChangesetUrl(window.location.href, settings.cps.changeset_id);
        if (url !== window.location.href) {
          window.history.replaceState(null, null, url);
        }

        // Rewrite links to preserve the current changeset. This is done
        // server-side in cps_url_outbound_alter() also but that will not catch
        // all links (for example, links embedded in user-generated content).
        $(context).find('a').each(function() {
          var $link = $(this);
          var href = $link.attr('href');
          // Only rewrite non-anchor links that point to the current site.
          if (href && href.charAt(0) != '#' && Drupal.urlIsLocal(href)) {
            var new_href = Drupal.CPS.getChangesetUrl(href, settings.cps.changeset_id, {'replace_existing': false});
            if (href !== new_href) {
              $link.attr('href', new_href);
            }
          }
        });
        // Because some links can be dynamically added to the page without
        // Drupal.behaviors running (for example, JavaScript which adds a link
        // entirely client-side), also check the link when it is clicked and
        // rewrite it then. This is used in addition to the above code rather
        // than instead of it because it is not as robust (on many browsers it
        // will not detect scenarios like right-clicking and choosing "Open
        // Link in New Tab", although it will detect keyboard shortcuts for
        // opening links in a new tab). This code is heavily copied from
        // Drupal.overlay.eventhandlerOverrideLink; see that function for
        // additional code comments.
        $(document).bind('click.cps-changeset mouseup.cps-changeset', function(event) {
          // Handle right-clicks correctly.
          if ((event.type == 'click' && event.button == 2) || (event.type == 'mouseup' && event.button != 2)) {
            return;
          }
          var $target = $(event.target);
          // Make sure this is a link.
          if (!$target.is('a')) {
            $target = $target.closest('a');
            if (!$target.length) {
              return;
            }
          }
          var target = $target[0];
          var href = target.href;
          // Skip non-links and non-HTTP(S) links.
          if (href == undefined || href == '' || !target.protocol.match(/^https?\:/)) {
            return;
          }
          // Skip anchor links.
          var anchor = href.replace(target.ownerDocument.location.href, '');
          if (anchor.length == 0 || anchor.charAt(0) == '#') {
            return;
          }
          // Skip links to other sites.
          if (!Drupal.urlIsLocal(href)) {
            return;
          }
          // If the link already has a changeset ID, do not rewrite it again.
          var new_href = Drupal.CPS.getChangesetUrl(href, settings.cps.changeset_id, {'replace_existing': false});
          if (href === new_href) {
            return;
          }
          // For normal clicks, override the click behavior and set the window
          // to the new location.
          if (event.button == 0 && !event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
            window.location.href = new_href;
            return false;
          }
          // When pressing a special mouse button or keyboard key (for example,
          // to open the link in a new window or tab) temporarily alter the
          // clicked link's href.
          else {
            $target
              .one('blur mousedown', { target: target, href: href }, function (event) { $(event.data.target).attr('href', event.data.href); })
              .attr('href', new_href);
          }
        });

        // Rewrite Ajax requests to preserve the current changeset.
        if (!Drupal.behaviors.cps.ajax_overridden) {
          Drupal.behaviors.cps.ajax_overridden = true;
          var originalXMLHttpRequestOpen = XMLHttpRequest.prototype.open;
          XMLHttpRequest.prototype.open = function() {
            // Rewrite the Ajax request URL to add the changeset ID. Ajax
            // requests can easily happen in a browser tab that is not active,
            // so tell the server to only override the changeset temporarily
            // for the current request (so it doesn't change the current active
            // editing session).
            arguments[1] = Drupal.CPS.getChangesetUrl(arguments[1], settings.cps.changeset_id, {'temporary_override': true, 'replace_existing': false});
            originalXMLHttpRequestOpen.apply(this, arguments);
          };
        }

        // Add a hidden element to forms to preserve the current changeset when
        // the form is submitted.
        $('form:not(#cps-changeset-preview-form)').once('cps-changeset-form', function () {
          $(this).append('<input type="hidden" name="changeset_id" value="' + settings.cps.changeset_id + '">');
        });
      }

      // Force a refresh when the changeset select box is changed.
      $('.form-select[name="changeset_id"]').once('changesetSelect').change(function() {
        var changeset_id = $(this).find(":selected").val();
        window.location.href = Drupal.CPS.getChangesetUrl(window.location.href, changeset_id);
      });
    }
  };
})(jQuery);
